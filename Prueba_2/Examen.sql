--se crea la Base de Datos llamadas
Create Database Llamadas
use Llamadas

-- Creamos la base logDial
create table logDial 
(
id int identity (1,1),
idLlamada  varchar(10),
fechaDeLlamada  datetime,
tiempoDialogo smallint,
telefono varchar(10),
tipoDeLlamada varchar(15)
)

-- Se crea la tabla costos
create table costos 
(
id int identity(1,1),
tipoDeLlamada varchar(15),
costo decimal(10,4)
)

-- Insertamos los datos a la tabla logDial de forma masiva consumiendo el archivos excel
INSERT INTO dbo.logDial
select * from OPENROWSET('Microsoft.ACE.OLEDB.12.0',
'Excel 12.0;HDR=YES;Database=C:\Users\myky2\Downloads\DatosPracticaSQL.xlsx;',
'SELECT * FROM [InfoLogDial$]');


--Insertamos los datos a la tabla costos de forma masiva consumiendo el archivos excel
INSERT INTO dbo.costos
select * from OPENROWSET('Microsoft.ACE.OLEDB.12.0',
'Excel 12.0;HDR=YES;Database=C:\Users\myky2\Downloads\DatosPracticaSQL.xlsx;',
'SELECT * FROM [InfoCostos$]');


--Que muestre los registros con tipo de llamada Cel LD durante el mes febrero
select *from [dbo].[logDial] where tipoDeLlamada = 'Cel LD'
and MONTH(fechaDeLlamada)=2

--Que indique el promedio de tiempo de dialogo de las llamadas con tipo Cel LD durante el mes de febrero
select AVG(tiempoDialogo) as 'Promedio de Tiempo Llamada' from [dbo].[logDial] where tipoDeLlamada = 'Cel LD'
and MONTH(fechaDeLlamada)=2

--Que muestre el n�mero en minutos de dialogo (tomando tiempoDialogo que est� en segundos) y 
--el costo de todas las llamadas del mes de enero
select (tiempoDialogo /60.0) as 'minutos', b.costo, tiempoDialogo, fechaDeLlamada, a.tipoDeLlamada from dbo.logDial as A
inner join dbo.costos as B
on B.tipoDeLlamada = A.tipoDeLlamada
where MONTH(fechaDeLlamada) =1
